/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!documentation/
 */


module.exports.policies = {

  // Default policy for all controllers and actions
  // (`true` allows public access)
  // Authentication required by default.
  '*': [ 'currentUser' ],

  // Allow all actions to be accessible by logged in users except find
  MeetController: {
    '*': [ 'logRequest', 'currentUser' ],
    find: [ 'logRequest'],
    create: ['currentUser', 'setCreator'],
    update: ['currentUser', 'isOwner'],
    destroy: ['currentUser', 'isOwner']
  },

  // Allow all actions to be accessible by logged in user except signin and sign up
  UserController: {
    '*': [ 'currentUser' ],
    signin: true,
    signup: true,
    renewToken: true,
    destroy: false,// We don't allow user to be deleted
    update: ['currentUser', 'isOwner'],
    appliedMeets: ['currentUser']
  },

  // Allow all actions to be accessible by logged in users except find
  CommentController: {
    '*': [ 'currentUser' ],
    create: ['currentUser', 'setCreator'],
    update: ['currentUser', 'isOwner'],
    destroy: ['currentUser', 'isOwner']
  },

  ProfileVisitController: {
    '*': 'currentUser',
    update: false,
    destroy: false,
    find: false
  },
  
  PhotoController: {
    '*': ['currentUser']
  }

};
