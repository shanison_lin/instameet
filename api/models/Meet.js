/**
 * Meet
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */
"use strict";

var LOCALITY_DELIMITER = "::";
module.exports = {
  maxAttendees: 3,

  /**
   * Custom types and validations
   */
  types: {

    // Limit the max attendees to the property maxAttendees
    limitedAttendees: function(attendees) {
      return attendees.length <= Meet.maxAttendees;
    },

    // Make sure that the attendees are within the list of applicants
    withinApplicants: function(attendees) {
      var lastAttendee = attendees.slice(-1)[0];
      return this.applicants && this.applicants.length > 0 && this.applicants.indexOf(lastAttendee) >= 0
    },

    // Make sure that the newly added Array element doesn't exists in the existing Array
    uniqElement: function(array) {
      var excludeLastArray = array.slice(0, -1);
      var lastElement = array.slice(-1)[0];
      return excludeLastArray.indexOf(lastElement) < 0;
    },

    isCoordinates: function(array) {
        return array.length === 2;
    }

  },

  attributes: {
    title: {
      type: "STRING",
      required: true
    },

    description: {
      type: "STRING"
    },

    photo: {
      type: "STRING"
    },

    /* guestType
     *
     * Potential Values
     * 0 : 1 man,
     * 1 : 1 woman,
     * 2 : either
     */
    guestType: 'INTEGER',

    /* billType
     *
     * Potential Values
     * 0 : on me
     * 1 : on you
     * 2 : split
     */
    billType: 'INTEGER',

    allowFriend: {
      type: 'BOOLEAN',
      defaultsTo: 0
    },

    time: {
      type: 'DATETIME',
      required: true
    },

    city: {
        type: 'STRING',
        required: true
    },

    geoCoordinates: {
        type: 'ARRAY',
        isCoordinates: true
    },

    location: 'JSON',

    // Add a reference to user
    creator: {
      model: 'user',
      required: true
    },

    // List of applicant object ids
    applicants: {
      type: 'ARRAY',
      uniqElement: true
    },

    confirmedAttendees: {
      type: 'ARRAY',
      limitedAttendees: true,
      withinApplicants: true,
      uniqElement: true
    },

    comments: {
      collection: 'comment',
      via: 'meet'
    },

    toJSON: function() {
        var obj = this.toObject();

        // We don't need to show the list of applicants and comments when retrieving meet detail
        // Instead, we just need the total count for applicants and comments
        obj.commentsCount = obj.comments ? obj.comments.length : 0;
        obj.applicantsCount = obj.applicants ? obj.applicants.length : 0;

        delete obj.comments;
        delete obj.applicants;
        delete obj.confirmedAttendees;

        // Delete those protected attributes from user
        if (obj.creator) {
            sails.models.user.protectedAttributes.forEach(function (attr) {
                delete obj.creator[attr];
            });
        }

        return obj;
    }


  },

  beforeValidate: function( meet, next ){
        var loc = meet.location;
        meet.geoCoordinates = [ loc.long, loc.lat ];
        meet.city = [ loc.country, loc.city ].join(LOCALITY_DELIMITER);
        sails.log(meet);
        next();
  }
};
