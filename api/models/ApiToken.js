/**
* ApiToken.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    token: {
      type: 'STRING',
      unique: true
    },

    user: {
      model: 'user',
      required: true
    },

    device: 'STRING',
    ip: 'STRING',
    expireAt: 'DATETIME'
  }
};

