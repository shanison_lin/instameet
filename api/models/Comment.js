/**
 * Comment
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {

    message: {
      type: 'STRING',
      required: true
    },

    // referrence to the meet model
    meet: {
      model : 'meet',
      required: true
    },

    // reference to the user model
    creator: {
      model : 'user',
      required: true
    },

    commentedComment: {
      model : 'comment'
    },

    toJSON: function() {
      var obj = this.toObject();

      // We don't need to show the meet details
      delete obj.meet;

      // Delete those protected attributes from user
      if (obj.creator) {
        sails.models.user.protectedAttributes.forEach(function(attr) {
          delete obj.creator[attr];
        })
      }
      return obj;
    }
  }

};
