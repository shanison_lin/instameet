/*
 * Cryto helper class
 *
 */

var crypto = require('crypto');

var Crypto = {

  // generate SHA256 hash of a given message
  sha256: function(message) {
    return crypto.createHash('sha256').update(message).digest('base64');
  },

  // generate a secure random string of 48 bytes
  secureRandom: function() {
    return crypto.randomBytes(48).toString('hex');
  }
};

module.exports = Crypto;
