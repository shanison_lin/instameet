/**
 * User
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

var merge = require("merge");
var _ = require("lodash");

var SECRET = _.constant("secret");
var DEFAULT = _.constant("defaultAvatar");

module.exports = {
  protectedAttributes: ['fbUserId', 'easemobPwd', 'createdAt', 'updatedAt', 'dob'],

  attributes: {
    fbUserId: {
      type: 'STRING',
      unique: true
    },

    firstName: {
      type: 'STRING',
      required: true
    },

    middleName: 'STRING',

    lastName: {
      type: 'STRING',
      required: true
    },

    email: {
      type: 'STRING',
      required: true,
      unique: true
    },

    hometown: 'STRING',

    gender: {
        type: 'STRING',
        defaultsTo: SECRET()
    },

    dob: 'DATE',

    // easemob related fields
    easemobUserName: {
      type: 'STRING'
    },

    easemobPwd: {
      type: 'STRING'
    },

    avatar: {
      type: 'STRING'
    },

    profilePhotos: {
      type: 'ARRAY'
    },

    description: {
      type: 'STRING'
    },
    /**
     * Instance methods
     */
    apiTokenForDevice: function(device, next) {
      // we assume that one device we give only one token
      ApiToken.findOne({device: device, user: this.id}).exec(function(err, apiToken){
        next(apiToken);
      });
    },

    // Find an existing valid token or create a new token
    findOrGenerateToken: function(ip, device, next) {
      var userId = this.id;

      this.apiTokenForDevice(device, function(apiToken){
        // if token not found, then we try to create one
        if (!apiToken) {
          // generate a random token and check if it is found
          // need to recursively check to make sure it is unique
          randomToken = Crypto.secureRandom();

          ApiToken.findOne({token: randomToken}).exec(function(err, found){
            // if no duplicate token found, then let's create a new API token
            if (!found) {
              ApiToken.create({
                token: randomToken,
                user:  userId,
                ip: ip,
                device: device
              }).exec(function (err, created){
                sails.log(created);
                next(created.token);
              });
            }
            else
            {
              next(found.token);
            }
          });
        }
        else {
          //if token is found, we directly return the token
          next(apiToken.token);
        }
      });
    },

    toJSON: function() {
      var obj = this.toObject();
      //Insert Calculated Fields.
      if(_.isDate(obj.dob)) {
          obj.age = new Date().getFullYear() - obj.dob.getFullYear();
      } else {
          obj.age = SECRET();
      }
      //Default avatar to the first profilePhoto.
      obj.avatar = obj.avatar || _.first(obj.profilePhotos) || DEFAULT();
      // If the user's currentUser flag is not set to true then we need to remove those sensitive attributes
      if (!obj.currentUser) {
        User.protectedAttributes.forEach(function(attr) {
          delete obj[attr];
        })
      }


      return obj;
    }
  },

  // A better design is that these methods are instance methods, however beforeCreate can't call those instance methods
  // http://stackoverflow.com/questions/19421253/how-can-i-call-a-model-instance-method-in-lifecycle-callback-in-sails-waterline
  generateEasemobUserName: function(record) {
    return 'instameet_' + record.id;
  },

  // return sha256 hash of the object id as the easemob password
  generateEasemobPwd: function(record) {
    return Crypto.sha256('instameet_' + record.id);
  },

  afterCreate: function (values, next) {
    updateAttributes = {
      easemobUserName: User.generateEasemobUserName(values),
      easemobPwd: User.generateEasemobPwd(values)
    };

    // update the user records with easemob username and password
    User.update(values, updateAttributes).exec(function (err, updated){
      if (err){
        sails.log("Failed to update record after create user");
      }
    });

    // merge the attributes with easemob username and password, if not, the easemob username and password won't be returned
    merge(values, updateAttributes);

    next();
  }

};
