/**
 * isOwner
 *
 * @module      :: Policy
 * @description :: Only allow owner to do update content created by themselves
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */
module.exports = function(req, res, next) {

  var validateOwner = function(model, currentUser, unpermittedMsg, next) {
    model.findOne({id: id, creator: currentUser}).exec(function(err, found){
      if (!found) {
        res.json(unpermittedMsg, 403);//403 Forbidden
      }
      else
        next();
    })
  };

  // User is allowed, proceed to the next policy,
  // or if this is the last policy, the controller
  var params = req.params.all();
  var id = params.id;
  var requestModel = req.options.model;
  var currentUser = req.session.currentUser;
  var unpermittedMsg = {error: 'You are not allowed to perform this action'};

  if (id && currentUser && requestModel) {
    // handling the case for user
    if (requestModel == 'user') {
      if (id == currentUser) {
        next();
      }
      else {
        res.json(unpermittedMsg, 403); // 403 Forbidden
      }
    }
    // handling the case for meet
    else if (requestModel == 'meet') {
      validateOwner(Meet, currentUser, unpermittedMsg, next);
    }
    // handling the case for comment
    else if (requestModel == 'comment') {
      validateOwner(Comment, currentUser, unpermittedMsg, next);
    }
    else
      next();
  }
  else
    next();
};
