/**
 * setCreator
 *
 * @module      :: Policy
 * @description :: Extract creator information from the token
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */

module.exports = function(req, res, next) {
  var params = req.params.all();
  var currentUser = req.session.currentUser;

  // if the param creator exists but is not equal to currentUser, then we throw error
  if (params.creator && params.creator != currentUser) {
    res.json({error: "Creator and current user not tally"}, 403); // 403 Forbidden
  }
  else {
    // default we set the creator to the currentUser
    params.creator = currentUser;
    next();
  }
};
