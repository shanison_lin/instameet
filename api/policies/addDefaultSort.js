/**
 * addDefaultSort
 *
 * @module      :: Policy
 * @description :: Simple policy to add some default sorting algorithms
 *                 The original ideas was to overwrite the controller methods, but the preferred way in V10.0 is using Policy
 *                 http://stackoverflow.com/questions/18110262/calling-super-method-in-sails-js-controller
 */

module.exports = function(req, res, next) {
  var params = req.params.all();
  var sort = params.sort;

  // If the sort is not added, then we add the default sort
  if (!sort) {
    // I have tried to use req.params.sort however, when it goes to controller, the req.params.sort value is gone
    req.options.sort = "createdAt desc";
  }

  return next();
}
