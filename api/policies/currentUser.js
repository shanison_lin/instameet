/**
 * currentUser
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any current logged in user to access resources
 */

module.exports = function(req, res, next) {

  var params = req.params.all();

  // try to find the token and its associated user
  ApiToken.findOne({token: params.token}).exec(function(err, token){
    // if token is not found, then we deny the access
    if (token && token.user) {
      req.session.currentUser = token.user;
      return next();
    }
    else {
      return res.json({
        error: 'You need to authenticate yourself with a valid token',
      }, 401); // 401 indicates Unauthorized
    }
  });
};
