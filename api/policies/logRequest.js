/**
 * logRequest.js
 *
 * @module      :: Policy
 * @description :: Simple policy to add some default sorting algorithms
 *                 The original ideas was to overwrite the controller methods, but the preferred way in V10.0 is using Policy
 *                 http://stackoverflow.com/questions/18110262/calling-super-method-in-sails-js-controller
 */

module.exports = function(req, res, next) {
   sails.log.debug(
        "Logging a request: " +
        JSON.stringify({
            header: req.headers,
            params: req.params,
            query: req.query,
            body: req.body,
            options: req.options
        })
   );
   return next();
};
