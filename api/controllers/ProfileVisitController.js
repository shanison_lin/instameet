/**
 * ProfileVisitController
 *
 * @description :: Server-side logic for managing Profilevisits
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  create: function(req, res, next) {
    var params = req.params.all();
    var currentUser = req.session.currentUser;
    var owner = params.owner;

    // Visitor is always the current user, who initiates the visits to the profile owner
    // We need to make sure visitor is present and visitor can't be the same as current user
    if (owner && owner != currentUser) {
      // We need to make sure profile owner is a valid user
      User.findOne({id: owner}).exec(function(err, user){
        if (user) {
          ProfileVisit.create({
            visitor: currentUser,
            owner: owner
          }).exec(function(err, visit){
            if (err) {
              res.json(err, 400);
            }
            else {
              res.json(visit, 200);
            }
          });
        }
        else {
          res.json({error: "Profile owner not found in database."}, 400)
        }
      })
    }
    else {
      res.json({error: "You need to provide the profile owner id and profile owner can't be yourself."}, 400)
    }
  }

};

