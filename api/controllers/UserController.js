/**
 * UserController
 *
 * @module      :: Controller
 * @description    :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
/*jslint node: true */
"use strict";

var request = require('request');
var Q = require('q');

//Private Helper functions

var getOneByQuery = function(res, query){
    User.findOne( query )
        .then( function( user ){
            sails.log( user );
            if( !user ) {
                throw new Error("No Such User");
            }
            res.json( user );
        }).catch( function( err ){
            sails.log.error( err );
            res.json( {error: err.message}, err.status || 404 );
        });
};

//noinspection JSUnusedGlobalSymbols
var UserController = {

  /**
   * Signin the user
   */
  signin: function(req, res, next) {
    UserController.signinOrSignup(req, res, next);
  },

  /**
   * New user sign up
   */
  signup: function(req, res, next) {
    UserController.signinOrSignup(req, res, next);
  },

  /**
   * Renew a new token
   *
   * Currently renew a new token logic is the same as sign in, which will find or generate a valid token
   * we use different routes first, just in case in future the logic is different
   */
  renewToken: function(req, res, next) {
    UserController.signinOrSignup(req, res, next);
  },

  visitors: function(req, res) {
    var params = req.params.all();
    var id = params.id;
    var currentUser = req.session.currentUser;

    if (id == currentUser) {
      // Use native query as sails don't support distinct statements now
      ProfileVisit.native(function(err, collection){
        collection.aggregate([
          {$sort: {"createdAt": 1}},
          {$group: {"_id": "$visitor", "createdAt": {$max: "$createdAt"}}},
          {$limit: 6}
        ], function(err, visitors){
          if (err) {
            res.json(err, 400);
          }
          else {
            // Map visitors objects to the visitors id
            visitors = visitors.map(function(visitor){
              return visitor._id;
            });

            // Map the visitor ids to actual user object
            User.find({id: visitors}).exec(function(err, users){
              if (err) {
                res.json(err, 400);
              }
              else
                res.json(users, 200);
            })
          }
        })
      })
    }
    else {
      res.json({error: "You are not allowed to check other user's visitors"}, 403);
    }
  },

  /**
   * Find all the events that the current user applied
   */
  appliedMeets: function(req, res) {
    var params = req.params.all();
    var id = params.id;
    var currentUser = req.session.currentUser;

    // We only allow the query of applied events for the current logged in user only for now
    if (id == currentUser) {
      Meet.find({applicants: currentUser}).exec(function(err, meets){
        if (err) {
          res.json(err, 400);
        }
        else {
          var meetSize = meets.length;
          // keep track of how many query has been done for the meet's creator
          var count = 0;

          if (meetSize > 0) {
            meets.forEach(function(meet, index){
              // TODO: optimize the query or use collection when sails.js is more rehanced
              User.findOne({id: meet.creator}).exec(function(err, user){
                count += 1;
                meet.creator = user;
                // We only return when it is the last item
                // Note that we can't use index as index won't be in oder of 0 to 5
                if (count == meetSize) {
                  sails.log("return");
                  res.json(meets, 200);
                }
              })
            })
          }
          else {
            res.json(meets, 200);
          }
        }
      })
    }
    else {
      res.json({error: "You are not permitted to do this"}, 400);
    }
  },

  /**
   * Reusable function for sign in and sign up
   * it will create new user if it doesn't exist or sign in the user if exists
   */
  signinOrSignup: function(req, res) {
    var params = req.params.all();
    var fbAccessToken = params.fbAccessToken;
    var device = params.device;
    var ip = req.connection.remoteAddress;

    var respondWithSuccess = function (user, ip, device) {
      user.findOrGenerateToken(ip, device, function(token){
        user.token = token;
        res.json(user, 200);
      });
    };

    if (fbAccessToken && fbAccessToken.length > 0 && device && device.length > 0) {

      // Verify the token with facebook
      UserController.verifyFbAccessToken(fbAccessToken).
        then(function(fbUser) {
          User.findOne({email: fbUser.email}).exec(function(err, found){

            // If user is not found, then we create the user
            if (!found) {
              User.create(fbUser).exec(function(err, user){
                if (err) {
                  res.json(err, 400)
                }
                else {
                  // add the attribute {newUser: true} if the user is newly created
                  user.newUser = true;

                  // we should not remove those protected attributes for the current logged in user
                  // so we should set the current user flag to true
                  user.currentUser = true;

                  respondWithSuccess(user, ip, device);
                }
              })
            }
            else {
              found.newUser = false;
              found.currentUser = true;
              respondWithSuccess(found, ip, device);
            }
          });
        }, function(error){
          res.json({error: error.message}, 400)
        })
    }
    else {
      sails.log('error', 'Bad login request from ' + req.connection.remoteAddress + '. Access Token and Device must be provided.');
      res.json({error: "Bad login request. Please provide access token and device."}, 400);
    }
  },

  getProfile: function(req, res){
  	  var requestedUserId = req.param( "id" ) || req.session.currentUser;
  	  getOneByQuery(res, { id: requestedUserId});
  },

    getByEmId: function( req, res ){
        getOneByQuery(res, { easemobUserName: req.param("id")});
    },
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to UserController)
   */
  _config: {},

  /**
   * Verify access token with fb, if it is valid, then it will return a object of user, which can be used directly to create a new user
   */
  verifyFbAccessToken: function(token) {
    var deferred = Q.defer();
    var path = "https://graph.facebook.com/me?access_token=" + token;
    request(path, function(error, response, body){
      var data = JSON.parse(body);
      if (!error && response && response.statusCode && response.statusCode == 200) {
        var user = {
          fbUserId: data.id,
          firstName: data.first_name,
          lastName: data.last_name,
          email: data.email,
          dob: data.birthday,
          gender: data.gender
        };

        // If the fb returns hometown country then we need to set it
        if (data.hometown && data.hometown.name)
          user.hometown = data.hometown.name;

        deferred.resolve(user);
      }
      else {
        sails.log(data.error);
        deferred.reject({code: response.statusCode, message: data.error.message});
      }
    });
    return deferred.promise;
  }

};

module.exports = UserController;
