/**
 * MeetController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
"use strict";

module.exports = {


  find: function(req, res) {
    var city = req.body.city||"",
        conditions = req.body.where || {},
        limit = req.body.limit || 10,
        skip = req.body.skip || 0,
        sort = req.body.sort || "createdAt DESC";

    sails.log.debug("Trying to find meets within " + city);
    sails.log.debug( JSON.stringify( {limit: limit, skip: skip, sort: sort}));
    Meet.find()
        .where( {city: {"startsWith": city}})
        .where( conditions )
        .sort(sort)
        .skip( skip )
        .limit( limit )
        .populate( "creator" )
        .exec( function(err, meets){
            if(err){
               res.json(err, err.status);
            } else {
               res.json(meets);
            }
        });
  },
  /**
   * Allow the current logged in user to apply for an event
   */
  apply: function(req, res, next) {
    var params = req.params.all();
    var id = params.id;
    var currentUser = req.session.currentUser;

    // We don't allow current user to apply his/her own event
    Meet.findOne({id: id, creator: { not: currentUser}}).exec(function(err, meet){
      // if meet not found, then we return error
      if (!meet) {
        res.json({error: 'Meet not found'}, 400);
      }
      else {
        // if applicants is not initialized then we set it to empty array first;
        if (!meet.applicants)
          meet.applicants = [];

        meet.applicants.push(currentUser);

        // save the meet with new applicant
        meet.save(function(err){
          if (err) {
            res.json(err, 400);
          }
          else {
            res.json(meet, 200);
          }
        })
      }
    })
  },

  /**
   * Allow the current logged in user to cancel apply for an event
   */
  cancelApply: function(req, res, next) {
    var params = req.params.all();
    var id = params.id;
    var currentUser = req.session.currentUser;

    // We don't allow current user to cancel apply his/her own event
    Meet.findOne({id: id, creator: { not: currentUser}}).exec(function(err, meet){
      // if meet not found, then we return error
      if (!meet) {
        res.json({error: 'Meet not found'}, 400);
      }
      else {
        // if applicants is not initialized then we set it to empty array first;
        if (!meet.applicants)
          meet.applicants = [];

        var index = meet.applicants.indexOf(currentUser);

        // if the current user is found in the applicants, we remove it then
        if (index != -1) {
          meet.applicants.splice(index, 1);

          // save the meet with new applicant
          meet.save(function(err){
            if (err) {
              res.json(err, 400);
            }
            else {
              res.json(meet, 200);
            }
          })
        }
        else {
          res.json({error: "You did not apply the meet"}, 400)
        }
      }
    })
  },

  /**
   * Allow the current logged in user to confirm the attendee of a meet
   * Note: Only the event creator can confirm his/her own events
   */
  confirm: function(req, res, next) {
    var params = req.params.all();
    var id = params.id;
    var currentUser = req.session.currentUser;
    var confirmedAttendee = params.confirmedAttendee;

    // We try to find the meet created by the user with the specific id
    Meet.findOne({id: id, creator: currentUser}).exec(function(err, meet){
      // if meet not found, then we return error
      if (!meet) {
        res.json({error: 'Meet not found'}, 400);
      }
      else {
        User.findOne(confirmedAttendee).exec(function(err, user){
          // if user is not found, then we return error
          if (!user) {
            res.json({error: 'Confirmed attendee cannot be found'}, 400);
          }
          else {
            // Initialize confirmedAttendees if it has not yet done so
            if (!meet.confirmedAttendees)
              meet.confirmedAttendees = [];

            meet.confirmedAttendees.push(confirmedAttendee);

            // save the meet with new applicant
            meet.save(function(err){
              if (err) {
                res.json(err, 400);
              }
              else {
                res.json(meet, 200);
              }
            })
          }
        })
      }
    })
  },

  /**
   * Find all confirmed attendees for a meet
   */
  attendees: function(req, res, next) {
    var params = req.params.all();
    var id = params.id;
    var currentUser = req.session.currentUser;

    // We only allow the user to query all confirmed attendees for the meets that are created by himself
    Meet.findOne({id: id, creator: currentUser}).exec(function(err, meet){
      if (err) {
        res.json(err, 400);
      }
      else {
        if (meet) {
          // Need to map the attendees ids to user records
          // TODO: See how we can enhance it using associations
          User.find({id: meet.confirmedAttendees}).exec(function(err, attendees){
            if (err) {
              res.json(err, 400);
            }
            else
              res.json(attendees, 200);
          })
        }
        else {
          res.json({error: "Meet not found"}, 400);
        }
      }
    })
  },

  /**
   * Find all applicants for a meet
   * TODO: extract some codes to be reused with attendees
   */
  applicants: function(req, res, next) {
    var params = req.params.all();
    var id = params.id;
    var currentUser = req.session.currentUser;

    // We only allow the user to query all applicants for the meets that are created by himself
    Meet.findOne({id: id, creator: currentUser}).exec(function(err, meet){
      if (err) {
        res.json(err, 400);
      }
      else {
        if (meet) {
          // Need to map the applicants ids to user records
          // TODO: See how we can enhance it using associations
          User.find({id: meet.applicants}).exec(function(err, applicants){
            if (err) {
              res.json(err, 400);
            }
            else
              res.json(applicants, 200);
          })
        }
        else {
          res.json({error: "Meet not found"}, 400);
        }
      }
    })
  },

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to MeetController)
   */
  _config: {}


};
