/**
 * PhotoController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
 "use strict";
 var Q = require('q');
 var _ = require("underscore");
 
 //Init File Uploading facilities
 var sid = require('shortid');
 var s3Credential = require('../../config/awsConfig.json');
 //Only used for local testing.
 
 //Init SkipperS3 Factory
 var skipperS3 = require( 'skipper-s3' )( {
	bucket: 'instameet.photo.us.std',
	key: s3Credential.accessKeyId,
	secret: s3Credential.secretAccessKey,
 } );
 //Init AWS SDK.
 var AWS = require('aws-sdk'); 
 AWS.config.loadFromPath('./config/awsConfig.json');
 var sts = new AWS.STS();
 /**Default Policy Template. 
 **	Replace ${userId} with the actual userID.
 **/
 var policyTemplate = JSON.stringify( {
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::instameet.photo/${userId}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": "s3:GetObject",
            "Resource": [
                "arn:aws:s3:::instameet.photo/*"
            ]
        }
    ]
} );
 
 function getUserPolicyString( userId ){
	return policyTemplate.replace( "${userId}", userId );
 }
 
/* function fileExtension(fileName) {
  return fileName.split('.').slice(-1);
}*/
module.exports = {
      
  /**
   * Action blueprints:
   *    `/photo/getToken`
   */
   getToken: function (req, res) {
		var currentUser = req.session.currentUser;
		var params = {
		  RoleArn: 'arn:aws:iam::347445277375:role/instameet.user', // required
		  RoleSessionName: "photo@" + currentUser, // required
		  Policy: getUserPolicyString( currentUser ) //Further Restrict User's Permission
		};
		console.log( "Assume Role with the following params: " + JSON.stringify( params ) );
		sts.assumeRole( params, function(err, data) {
			if (err) {
				console.log(err, err.stack);
				res.json( { status : 'failed to get access token', error : err } );
			} else {
				res.json(data);
			}
		} );
  },
  
   upload : s3Upload,

    /**
     * delete a particular photo.
      * @param req
     * @param res
     */
   delete : function( req, res) {
       var currentUser = req.session.currentUser,
            fileId = req.param( "fileId" );
       User.findOne( currentUser)
           .where( {id: currentUser} )
           .then( function onDone( user ){
               var deferred = Q.defer();
               if(user.avatar === fileId) {
                   throw new Error( "Cannot delete avatar, change a avatar first");
               }
               if(!_.contains(user.profilePhotos, fileId)) {
                   throw new Error( "No such photo" );
               }
               if(_.size(user.profilePhotos) < 2 ){
                   throw new Error( "Please leave at least one photo" );
               }
               user.profilePhotos = _.without(user.profilePhotos, fileId);
               user.save( function onError( err ){
                   if( err ) {
                       deferred.reject( err );
                   } else {
                       deferred.resolve();
                   }
               });
               return deferred.promise;
           })
           .then(function onDone(){
               sails.log( "Successfully Removed Photo: " + fileId );
               res.json( {status: "success" });
           })
           .catch(function onError( err ){
               sails.log( "Failed to delete. Error: " + err);
               res.json( {error: err.message}, 403 );
           });
   },

    /**
     * Set the Avatar(Picture) of the User.
     * @param req
     * @param res
     */
    setAvatar: function( req, res ) {
        var currentUser = req.session.currentUser,
             newAvatar = req.param( "newAvatar");

       User.findOne()
            .where({id: currentUser})
            .then(function onDone( user ) {
                if(!_.contains(user.profilePhotos, newAvatar)) {
                    throw new Error("Avatar must be one of the profilePhotos: " + user.profilePhotos );
                }
                user.avatar = newAvatar;
                user.save( function onSave( err ){
                    if(err) { throw err;}
                    res.json( {status: "success"} );
                });
             })
            .catch(function onError( err ){
                res.json( {status: "fail", error: err.message } );
            });
   },
  
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to PhotoController)
   */
  _config: {}
  
};

 function s3Upload(req, res) {
	var currentUser = req.session.currentUser,
        fileId = currentUser + "_" + sid.generate();
	//Do not timeout with the request.
	res.setTimeout(0);
	
	//Upload both the file and thumbnail;
	Q.all([
		User.findOne().where({ id: currentUser }),
		uploadWithPromise( req.file("photo"), fileId ),
		uploadWithPromise( req.file("tn"), fileId + ".tn" )
	])
		.spread( function onDone( user, photo, tn ){
			sails.log(photo);
			sails.log(tn);
			sails.log(user);
			//Add uploaded photo to user's profile photo list.
			if(!user.profilePhotos){
                user.profilePhotos = [];
            }
			user.profilePhotos.push(fileId);
			user.save(function( err ){
				if( err ) {return res.json( {error: "Cannot Update User" } );}
				res.json( {status: "success", fileId: fileId, userPhotos: user.profilePhotos} );
			});
		})
		.catch(  function onError( err ) {
				res.serverError( err );
			})			
		.done(); 
  }
  
  /**
  * Expose the req.file( 'id' ).upload as a Promise.
  */
  function uploadWithPromise(file, targetFileId) {
  	var deferred = Q.defer(),
  	     receiving = skipperS3.receive({
  		    dirname: "",
  		    saveAs:	function(file){ return targetFileId; }
  	     });
  	
  	file.upload(receiving, function whenDone(err, uploadFiles){
  			if(err) deferred.reject(err)
  			else deferred.resolve(uploadFiles);
  		}
  	);
  	
  	return deferred.promise;	
  }
  
     /**
   * Action blueprints:
   *    `/photo/upload`
   */
  function fileUpload (req, res) {
    res.setTimeout(0);
    req.file('userPhoto')
		.upload({
			// You can apply a file upload limit (in bytes)
			maxBytes: 2000000
		}, function whenDone(err, uploadedFiles) {
			if (err) return res.serverError(err);
			else return res.json({
				files: uploadedFiles,
				textParams: req.params.all()
		});
    });
	
	req.file('tn')
		.upload(
		{}, function whenDone(err, uploadedFiles){
			if (err) console.error( "Failed to upload 2nd file. Error:", err );
			else {
				console.log( "Sucessfully uploaded 2nd file." );
				console.log( uploadedFiles );
			}
		}
	    );
	
  }
