console.log('get started')

/* after get access token, can use to send message as long as we know the username 
   for instance we have a token as :
{ access_token: 'YWMtXpi2rAZuEeSSV_0ZGtnqZgAAAUc41_YPCShd2zsuTn6NLRCB0zGdzry8Xn8',
  expires_in: 604800,
  application: 'bd6f1d60-038f-11e4-89b7-6d759435c0eb' }
*/

var request = require('request');
var options = {
  uri: 'https://a1.easemob.com/instameet/instameet/messages',
  method: 'POST',
  headers: {
    'Authorization': 'Bearer YWMtk701tBM6EeSxILfO9Ww1ZQAAAUeMtupLU3hMXFZYKBWB4xrVtgiLSPEKqU8',
  },
  json: {
      "target_type":"users",
      "target":["shanison","kyle","howard","bob"],
      "msg":{
          "type":"txt",
          "msg":"welcome to instameet, this is a message generated via instameet server side"
      },
      "from":"instameet"
   }

};

request(options, function (error, response, body) {
  if (!error && response.statusCode == 200) {
    console.log(body)  //message sent return result
  }
  else {
    console.log('got error');
  }
});

